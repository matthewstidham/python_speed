#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>

void change_array(int arr[], int value){
    arr[value] = value;
}

int main() {
    int limit = 1000000;

    printf("The following are the prime numbers smaller than or equal to %d\n", limit);
    int prime[1000000];
    int SizeOfPrime= sizeof(prime) / sizeof(prime[0]);
    for (int i = 0; i < SizeOfPrime; i++) {
        change_array(prime, i);
    }
    for (int i = 2; i < SizeOfPrime / 2; i++) {
        int end = prime[i];
        if (end == 0) {
            continue;
        }
        printf("i: %d, end: %d\n", i, end);
        for (int i = end + end; i < SizeOfPrime; i+=end) {
            //printf("%d\n", i);
            prime[i] = 0;
            if (prime[i] == 0) {
                continue;
            }
        }
    }
    printf("prime numbers: ");
    int NewSize= sizeof(prime) / sizeof(prime[0]);
    for (int loop = 0; loop < NewSize; loop++) {
        if (prime[loop] != 0) {
            printf("%d ", prime[loop]);
        }
    }
    printf("\n");
    return 0;
}
