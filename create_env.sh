#!/usr/bin/env bash

for VAR in 3.9 3.10 3.11 3.12 3.13;
do
  "python${VAR}" -m venv "py${VAR}"
done
