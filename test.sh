#!/usr/bin/env bash

envs=("py3.9" "py3.10" "py3.11" "py3.12" "py3.13")

for env in "${envs[@]}"
do
  echo "${env}" >> time.txt
  source "${env}/bin/activate"
  { time ./eratosthenes.py ; } 2> temp.txt
  cat temp.txt >> time.txt
  echo " " >> time.txt
  rm temp.txt
done

echo "C" >> time.txt
{ time ./eratosthenes ; } 2> temp.txt
cat temp.txt >> time.txt
rm temp.txt
