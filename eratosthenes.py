#!/usr/bin/env python3

# Python program to print all Primes Smaller
# than or equal to N using Sieve of Eratosthenes


def sieve_of_eratosthenes(limit, printer=False):
    prime = [True for i in range(limit + 1)]
    # boolean array
    p = 2
    while p * p <= limit:

        # If prime[p] is not
        # changed, then it is a prime
        if prime[p] is True:

            # Updating all multiples of p
            for i in range(p * p, limit + 1, p):
                prime[i] = False
        p += 1

    primes = [1]

    if printer:
        # Print all prime numbers
        for p in range(2, limit + 1):
            if prime[p]:
                primes.append(p)
        print(primes)


# Driver code
if __name__ == '__main__':
    num = 10 ** 6
    print("Following are the prime numbers smaller"),
    print("than or equal to", num)
    sieve_of_eratosthenes(num, printer=True)
